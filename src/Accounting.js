import React from "react";
import BarChart from "react-svg-bar-chart";
import PieChart from "react-minimal-pie-chart";
import AccountingRectangle from "./AccountingRectangle";
import ComponentSideRectangle from "./ComponentSideRectangle";

export default class Accounting extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cashData: [
        { title: "Expence 1", amount: 11000 },
        { title: "Expence 2", amount: 30000 },
        { title: "Expence 3", amount: 24100 },
        { title: "Expence 4", amount: 20000 },
        { title: "Expence 5", amount: 32000 }
      ],
      revenueData: [
        { title: "Revenue 1", amount: 11000 },
        { title: "Revenue 2", amount: 30000 },
        { title: "Revenue 3", amount: 24100 },
        { title: "Revenue 4", amount: 20000 }
      ],
      invoiceData: [
        { title: "Invoice 1", amount: 11000 },
        { title: "Invoice 2", amount: 30000 },
        { title: "Invoice 3", amount: 24100 },
        { title: "Invoice 4", amount: 20000 },
        { title: "Invoice 5", amount: 32000 },
        { title: "Invoice 6", amount: 320000 }
      ],
      billData: [
        { title: "Bill 1", amount: 11000 },
        { title: "Bill 2", amount: 30000 }
      ],
      chartData: [
        { x: 1, y: 11000 },
        { x: 2, y: 30000 },
        { x: 3, y: 24100 },
        { x: 4, y: 20000 },
        { x: 5, y: 32000 }
      ],
      piedata: [
        { title: "One", value: 10, color: "#DAA520" },
        { title: "Three", value: 30, color: "#FFB6C1" },
        { title: "Three", value: 40, color: "#00BFFF" },
        { title: "Three", value: 20, color: "#7B68EE" }
      ]
    };
  }

  render() {
    let { chartData, piedata } = this.state;
    return (
      <div className="container well">
        <div className="col-md-9">
          <div className="panel">
            <div className="panel-heading text-left">
              <h3>Accounting</h3>
            </div>
            <div className="text-left panel-body">
              <div className="col-md-12">
                <div className="row well">
                  <div className="col-md-6">
                    <div className="panel">
                      <div
                        className="panel-heading text-left"
                        style={{ backgroundColor: "#F0FFF0" }}
                      >
                        <h4>Income and Expence</h4>
                      </div>
                      <div className="panel-body">
                        <BarChart data={chartData} />
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="panel">
                      <div
                        className="panel-heading text-left"
                        style={{ backgroundColor: "#F0FFF0" }}
                      >
                        <h4>Business Expencess</h4>
                      </div>
                      <div className="panel-body">
                        <PieChart style={{ height: 118 }} data={piedata} />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row well">
                  <div className="col-md-6">
                    <AccountingRectangle
                      title={"Cash & Bank Balance"}
                      tableData={this.state.cashData}
                    />
                  </div>
                  <div className="col-md-6">
                    <AccountingRectangle
                      title={"Revenue"}
                      tableData={this.state.revenueData}
                    />
                  </div>
                </div>
                <div className="row well">
                  <div className="col-md-6">
                    <AccountingRectangle
                      title={"Invoice Payable"}
                      tableData={this.state.invoiceData}
                    />
                  </div>
                  <div className="col-md-6">
                    <AccountingRectangle
                      title={"Bill you need to pay"}
                      tableData={this.state.billData}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-3">
          <ComponentSideRectangle value="acc" />
        </div>
      </div>
    );
  }
}
