import React from "react";

export default class CrmRectangle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      item: {
        text: "",
        textColor: ""
      },
      number: ""
    };
  }
  componentDidMount() {
    let text = this.props.data;
    let employeeData = {
      text: "Employee",
      textColor: "#008000"
    };
    let companyData = {
      text: "Company",
      textColor: "#800080"
    };
    if (text === "employee") {
      this.setState({ item: employeeData,number:45 });
    } else if (text === "company") {
      this.setState({ item: companyData,number:"20" });
    }
  }
  componentWillReceiveProps() {
    let item = this.props.data;
    this.setState({ item: item});
  }
  render() {
    let stateObj = this.state;
    let {number} = this.state;
    let color = stateObj.item.textColor;
    return (
      <div className="panel">
        <div className="panel-body">
          <div className="col-md-6">
            <div className="row text-center">
              <span style={{ color: color, fontSize: 35 }}>{number}</span>
            </div>
            <div className="row">
              <span>{this.state.item.text}</span>
            </div>
          </div>
          <div className="col-md-6">
            <div className="row text-left">
              <svg height="15" width="15">
                <circle
                  cx="10"
                  cy="10"
                  r="5"
                  strokeWidth="3"
                  fill="#00FFFF"
                />
              </svg>
              <span> 0 Customers</span>
            </div>
            <div className="row text-left">
              <svg height="15" width="15">
                <circle
                  cx="10"
                  cy="10"
                  r="5"
                  strokeWidth="3"
                  fill="#7B68EE"
                />
              </svg>
              <span> 15 Lead</span>
            </div>
            <div className="row text-left">
              <svg height="15" width="15">
                <circle
                  cx="10"
                  cy="10"
                  r="5"
                  strokeWidth="3"
                  fill="#0000FF"
                />
              </svg>
              <span> 8 Opportunity</span>
            </div>
          </div>
        </div>
        <div
          className="panel-footer"
          style={{ backgroundColor: "blue", color: "white" }}
        >
          <input
            type="button"
            value={"View all " + stateObj.item.text + "s"}
            style={{
              backgroundColor: "blue",
              color: "white",
              border: "none"
            }}
          />
        </div>
      </div>
    );
  }
}
