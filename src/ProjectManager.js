import React from "react";
import ProjectRectangle from "./ProjectRectangle";
import ComponentSideRectangle from "./ComponentSideRectangle";

export default class ProjectManager extends React.Component {
  render() {
    return (
      <div className="container well">
        <div className="col-md-3">
          <ComponentSideRectangle value="project" />
        </div>
        <div className="col-md-9">
          <div className="panel">
            <div className=" panel-heading text-left">
              <h3>Project Manager</h3>
            </div>
            <div className="panel-body">
              <div className="panel row">
                <div className="panel-body">
                  <div className="col-md-2">
                    <button type="button" className="btn btn-primary">
                      <i className="fas fa-plus" /> New Project
                    </button>
                  </div>
                  <div className="col-md-4">
                    <input type="text" style={{ backgroundColor: "#E6E6FA" }} />
                    <input
                      type="button"
                      className="btn btn-primary"
                      value="Filter"
                      style={{ marginLeft: 15 }}
                    />
                  </div>
                  <div className="col-md-6">
                    <input type="text" style={{ backgroundColor: "#E6E6FA" }} />
                    <input
                      type="text"
                      style={{ backgroundColor: "#E6E6FA", marginLeft: 20 }}
                    />
                  </div>
                </div>
                <div
                  className="panel-footer text-left"
                  style={{ backgroundColor: "#F0FFF0" }}
                >
                  <button type="button" className="btn">
                    <i className="fas fa-times-circle" /> All
                  </button>
                  <button
                    type="button"
                    className="btn"
                    style={{ marginLeft: 15 }}
                  >
                    <i className="fas fa-arrow-down" /> Active
                  </button>
                  <button
                    type="button"
                    className="btn"
                    style={{ marginLeft: 15 }}
                  >
                    <i className="fas fa-check-circle" /> Completed
                  </button>
                </div>
              </div>
              <div className="well">
                <div className="row">
                  <ProjectRectangle
                    name={"Project 1"}
                    brief={"Project 1 Brief"}
                  />
                  <ProjectRectangle
                    name={"Project 2"}
                    brief={"Project 2 Brief"}
                  />
                  <ProjectRectangle
                    name={"Project 3"}
                    brief={"Project 3 Brief"}
                  />
                </div>
                <br />
                <div className="row">
                  <ProjectRectangle
                    name={"Project 4"}
                    brief={"Project 4 Brief"}
                  />
                  <ProjectRectangle
                    name={"Project 5"}
                    brief={"Project 5 Brief"}
                  />
                  <ProjectRectangle
                    name={"Project 6"}
                    brief={"Project 6 Brief"}
                  />
                </div>
                <br />
                <div className="row">
                  <ProjectRectangle
                    name={"Project 7"}
                    brief={"Project 7 Brief"}
                  />
                  <ProjectRectangle
                    name={"Project 8"}
                    brief={"Project 8 Brief"}
                  />
                  <ProjectRectangle
                    name={"Project 9"}
                    brief={"Project 9 Brief"}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
