import React from "react";
import ComponentSideChildRectangle from "./ComponentSideChildRectangle";

export default class ComponentSideREctangle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      heading: "",
      description: "",
      childData: {
        h1: "Evolving Database",
        h2: "Keep a keen eye",
        h3: "Get organized",
        description: "Track / Import employee attendaness and manage their leaves. Make important announcements when you need everyone to is heard."
      }
    };
  }
  componentDidMount() {
    let self = this;
    let value = this.props.value;
    if (value === "hrm") {
      self.setState({
        heading: "Empower Your Workforce with HRM",
        description:
          " A FREE WordPress HRM Plugin that provides Easy Employee Management for your company. Detailed Employee Profile, Leave Request, Promotion, Increment, Performance Logs, Announcement, Email Notification, HR Report and more!",
        childData: {
          h1: "Evolving Database",
          h2: "Keep a keen eye",
          h3: "Get organized",
          description: "Track / Import employee attendaness and manage their leaves. Make important announcements when you need everyone to is heard."
        }
      });
    } else if (value === "crm") {
      self.setState({
        heading: "Every Customer is Important in CRM",
        description:
          "Free WordPress CRM Plugin – No Limit On Anything! Manage your contacts like a pro with easy filtering and live stages, send emails, schedule meetings, calls, create tasks, create segments and much more."
      });
    } else if (value === "acc") {
      self.setState({
        heading: "Get Balanced With Accounting",
        description:
          "Take total control of your company’s finances in real time with the Accounting module. You can generate live and detailed reports and make transactions, generate invoices and pay employees all from a single place."
      });
    } else if (value === "project") {
      self.setState({
        heading: "Integrated Project Management System",
        description:
          "No need to use expensive solutions like Basecampor any other task and project management system. We got you covered with everything you might need."
      });
    }    
  }

  render() {
    let { heading, description, childData } = this.state;
    return (
      <div className="well text-left">
        <h3>{heading}</h3>
        <p>{description}</p>
        <div>
            <ComponentSideChildRectangle data={childData} />
        </div>
        <div>
          <input type="button" value="View Details" className="btn" />
        </div>
      </div>
    );
  }
}
