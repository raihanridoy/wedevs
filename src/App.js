import React, { Component } from "react";
// import logo from './logo.svg';
import "./App.css";
import Home from "./Home";
import {Route, BrowserRouter as Router } from "react-router-dom";
import HRManagement from "./HRManagement";
import CRM from "./CRM";
import Accounting from "./Accounting"
import ProjectManager from "./ProjectManager";
import "./Assets/css/default.min.css";
import Header from "./Header"
import Footer from "./Footer";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <div>
              <Header />
              <Route path="/" exact component={Home} />
              <Route path="/hrm" component={HRManagement} />
              <Route path="/crm" component={CRM} />
              <Route path="/accounting" component={Accounting} />
              <Route path="/projectmanager" component={ProjectManager} />
              <Footer />
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
