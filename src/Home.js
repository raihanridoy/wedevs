import React from "react";
import SideBar from "./SideBar";
import HRManagement from "./HRManagement";
import CRM from "./CRM";
import ProjectManager from "./ProjectManager";
import Accounting from "./Accounting";
import "./Assets/css/default.min.css";
import Review from "./Review";

export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isHrmShown: true,
      isCrmShown: true,
      isAccShown: true,
      isPmShown: true
    };
    this.click = this.click.bind(this);
  }
  click(event) {
    let value = event.target.value;
    if (value === "Home") {
      this.setState({
        isHrmShown: true,
        isCrmShown: true,
        isAccShown: true,
        isPmShown: true
      });
    } else if (value === "HR") {
      this.setState({
        isHrmShown: true,
        isCrmShown: false,
        isAccShown: false,
        isPmShown: false
      });
    } else if (value === "CRM") {
      this.setState({
        isHrmShown: false,
        isCrmShown: true,
        isAccShown: false,
        isPmShown: false
      });
    } else if (value === "Accounting") {
      this.setState({
        isHrmShown: false,
        isCrmShown: false,
        isAccShown: true,
        isPmShown: false
      });
    } else if (value === "Projects") {
      this.setState({
        isHrmShown: false,
        isCrmShown: false,
        isAccShown: false,
        isPmShown: true
      });
    }
  }

  render() {
    let { isHrmShown, isCrmShown, isAccShown, isPmShown } = this.state;
    return (
      <div style={{ marginTop: 10 }}>
        <div className="col-md-1">
          <SideBar click={this.click} />
        </div>
        <div className="col-md-10" style={{ marginLeft: 15 }}>
          {isHrmShown && (
            <div className="row">
              <HRManagement />
            </div>
          )}
          {isCrmShown && (
            <div className="row">
              <CRM />
            </div>
          )}
          {isAccShown && (
            <div className="row">
              <Accounting />
            </div>
          )}
          {isPmShown && (
            <div className="row">
              <ProjectManager />
            </div>
          )}
        </div>
        <div>
          <Review />
        </div>
      </div>
    );
  }
}
