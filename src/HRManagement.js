import React from "react";
import PieChart from "react-minimal-pie-chart";
import DayPicker from "react-day-picker";
import "react-day-picker/lib/style.css";
import HRMRectangle from "./HRMRectangle"
import "./Assets/css/default.min.css";
import ComponenetSideRectangle from "./ComponentSideRectangle";

const birthdayStyle = `.DayPicker-Day--highlighted {
    background-color: orange;
    color: white;
  }`;

const leaveStyle = `.DayPicker-Day--leave {
    background-color: red;
    color: white;
  }`;

const modifiers = {
  highlighted: new Date(),
  leave: new Date(2019, 1, 19)
};

export default class HRMnanagement extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        { title: "One", value: 80, color: "#32CD32" },
        { title: "Three", value: 20, color: "#FF4500	" }
      ]
    };
    this.changeSelect = this.changeSelect.bind(this);
  }

  render() {
    return (
      <div className="container well">
        <div className="col-md-9">
        <div className="panel">
          <div className="panel-heading text-left">
            <h3>HR Mnanagement</h3>
          </div>
          <div className="panel-body">
            <div className="col-md-8">
              <div className="row well">
                <div className="col-md-4">
                  <HRMRectangle text={"Employee"} count={"45"} textColor={"#228B22"} />
                </div>
                <div className="col-md-4">
                <HRMRectangle text={"Department"} count={"20"} textColor={"#DAA520"} />
                </div>
                <div className="col-md-4">
                <HRMRectangle text={"Designation"} count={"40"} textColor={"#800080"} />
                </div>
              </div>
              <div className="row">
                <div className="panel">
                  <div
                    className="panel-heading text-left"
                    style={{ backgroundColor: "#F0FFF0" }}
                  >
                    <h4>
                      <i className="fas fa-volume-up" /> Latest Anouncement
                    </h4>
                  </div>
                  <div
                    className="panel-body"
                    style={{ backgroundColor: "#f5f5f5" }}
                  >
                    <div className="row">
                      <div className="col-md-2">Heading</div>
                      <div className="col-md-10 text-left">Date</div>
                    </div>
                    <div className="row">
                      <div className="col-md-2">Heading</div>
                      <div className="col-md-10 text-left">Date</div>
                    </div>
                    <div className="row">
                      <div className="col-md-2">Heading</div>
                      <div className="col-md-10 text-left">Date</div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="panel">
                  <div
                    className="panel-heading text-left"
                    style={{ backgroundColor: "#F0FFF0" }}
                  >
                    <h4>
                      <i className="fas fa-calendar-alt" /> My leave calendar
                    </h4>
                  </div>
                  <div
                    className="panel-body"
                    style={{ backgroundColor: "#f5f5f5" }}
                  >
                    <div  >
                      <style>{birthdayStyle}</style>
                      <style>{leaveStyle}</style>
                      <DayPicker
                        onDayClick={this.onDayChange}
                        modifiers={modifiers}
                        month={new Date()}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="container col-md-3 well" style={{ marginLeft: 30 }}>
              <div className="panel">
                <div
                  className="panel-heading"
                  style={{ backgroundColor: "#F0FFF0" }}
                >
                  <h4>
                    <i className="fas fa-birthday-cake" /> Birthday
                  </h4>
                </div>
                <div className="panel-body">
                  <h6 className="text-left">This Month</h6>
                  <div className="row">
                    <div className="col-md-4">Name</div>
                    <div className="col-md-8 text-left">1/16/2019</div>
                  </div>
                  <div className="row">
                    <div className="col-md-4">Name</div>
                    <div className="col-md-8 text-left">1/17/2019</div>
                  </div>
                  <div className="row">
                    <div className="col-md-4">Name</div>
                    <div className="col-md-8 text-left">1/23/2019</div>
                  </div>
                </div>
              </div>
              <div className="panel">
                <div
                  className="panel-heading"
                  style={{ backgroundColor: "#F0FFF0" }}
                >
                  <h4>
                    <i className="fas fa-user" /> Who is out
                  </h4>
                </div>
                <div className="panel-body">
                  <h6 className="text-left">This Month</h6>
                  <div className="row">
                    <div className="col-md-4">Name</div>
                    <div className="col-md-8 text-left">1/16/2019</div>
                  </div>
                  <div className="row">
                    <div className="col-md-4">Name</div>
                    <div className="col-md-8 text-left">1/17/2019</div>
                  </div>
                  <div className="row">
                    <div className="col-md-4">Name</div>
                    <div className="col-md-8 text-left">1/23/2019</div>
                  </div>
                </div>
              </div>
              <div className="panel">
                <div
                  className="panel-heading"
                  style={{ backgroundColor: "#F0FFF0" }}
                >
                  <h4>
                    <i className="fas fa-chart-pie" /> Attendance Status
                  </h4>
                </div>
                <div className="panel-body">
                  <div className="text-left">
                    <label>Filter by</label>
                    <select
                      onChange={this.changeSelect}
                      style={{ marginLeft: 10 }}
                    >
                      <option value="sat">Sat</option>
                      <option value="sun">Sun</option>
                    </select>
                  </div>
                  <div style={{marginTop:15}}>
                    <PieChart style={{ height: 150 }} data={this.state.data} />
                  </div>
                </div>
                <div className="text-left">
                  <svg style={{ width: 15, height: 15, color: "blue" }}>
                    <rect fill="#32CD32	" style={{ width: 100, height: 100 }} />
                  </svg>
                  <span> Present</span>
                  <br />
                  <svg
                    width="15"
                    height="15"
                    color="blue"
                    backgroundColor="blue"
                  >
                    <rect
                      fill="#FF4500"
                      style={{ width: 100, height: 100}}
                    />
                  </svg>
                  <span> Absent</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
        <div className="col-md-3">
          <ComponenetSideRectangle value="hrm" />
        </div>
      </div>
    );
  }

  changeSelect(e) {
    let date = e.target.value;
    let sat = [
      { title: "One", value: 80, color: "#32CD32" },
      { title: "Three", value: 20, color: "#FF4500	" }
    ];
    let sun = [
      { title: "One", value: 90, color: "#32CD32" },
      { title: "Three", value: 10, color: "#FF4500	" }
    ];
    if (date === "sun") {
      this.setState({
        data: sun
      });
    }
    if (date === "sat") {
      this.setState({
        data: sat
      });
    }
  }

  onDayChange(e) {
    let date = new Date(e);
    console.log(date);
    //will call an api to get specific dates leave
  }  
}
