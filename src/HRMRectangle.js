import React from "react";

export default class HRMRectangle extends React.Component {
  // constructor(props) {
  //   super(props);
  // }
  render() {
    let color = this.props.textColor;
    return (
      <div className="panel">
        <div className="panel-body">
          <div className="row text-center">
            <span style={{ color: color, fontSize: 35 }}>
              {this.props.count}
            </span>
          </div>
          <div className="row">
            <span>{this.props.text}</span>
          </div>
        </div>
        <div
          className="panel-footer"
          style={{ backgroundColor: "blue", color: "white" }}
            >
          <input
            type="button"
            value={ this.props.text+"s"}
            style={{
              backgroundColor: "blue",
              color: "white",
              border: "none"
            }}
          />
        </div>
      </div>
    );
  }
}
