import React from "react";

export default class ComponentSideChildRectangle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      h1: "",
      h2: "",
      h3: "",
      description: ""
    };
  }
  componentDidMount() {
    let data = this.props.data;
    this.setState({
      h1: data.h1,
      h2: data.h2,
      h3: data.h3,
      description: data.description
    });
  }
  render() {
    let { h1, h2, h3, description } = this.state;
    return (
      <div className="row">
        <div className="panel">
          <h3>
            <svg height="20" width="20" style={{ marginLeft: 5 }}>
              <circle cx="10" cy="10" r="10" strokeWidth="3" fill="purple" />
            </svg>{"  "}
            {h1}
          </h3>
        </div>
        <div className="panel">
          <h3>
            <svg height="20" width="20" style={{ marginLeft: 5 }}>
              <circle cx="10" cy="10" r="10" strokeWidth="3" fill="aqua" />
            </svg>{"  "}
            {h2}
          </h3>
          <p>{description}</p>
        </div>
        <div className="panel">
          <h3>
            <svg height="20" width="20" style={{ marginLeft: 5 }}>
              <circle cx="10" cy="10" r="10" strokeWidth="3" fill="brown" />
            </svg>{"  "}
            {h3}
          </h3>
        </div>
      </div>
    );
  }
}
