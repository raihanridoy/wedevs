import React from "react";

export default class CRMSideRectangle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: "",      
      schedules: this.props.schedules || []
    };
  }
  componentDidMount() {
    let text = this.props.data;
    if (text === "today") {
      let schedules = [
        {
          name: "Raihan",
          time: "11.00 pm"
        },
        {
          name: "Ridoy",
          time: "3.00 pm"
        },
        {
          name: "Other",
          time: "5.00 pm"
        }
      ];
      this.setState({
        text: "Todays Schedule",
        schedules: schedules
      });
    } else if (text === "comming") {
      let schedules = [
        {
          name: "Raihan",
          time: "24/01/2019 11.00 pm"
        },
        {
          name: "Ridoy",
          time: "25/01/2019 3.00 pm"
        },
        {
          name: "Other",
          time: "24/01/2019 5.00 pm"
        }
      ];
      this.setState({
        text: "Upcomming Schedule",
        schedules: schedules
      });
    }
  }
  render() {
    let { text, schedules } = this.state;
    return (
      <div className="panel">
        <div
          className="panel-heading text-left"
          style={{ backgroundColor: "#F0FFF0" }}
        >
          <h4>
            <i className="fas fa-clock" /> {text}
          </h4>
        </div>
        <div className="panel-body">

          {schedules.map((appointment, index) => {
            return (
              <div key={index} className="row text-left">
                <div className="col-md-4">{appointment.name}</div>
                <div className="col-md-8 text-left">{appointment.time}</div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}
