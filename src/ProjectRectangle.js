import React from "react";

export default class ProjectRectangle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      brief: ""
    };
  }
  componentDidMount() {
    let name = this.props.name;
    let brief = this.props.brief;
    this.setState({
      name: name,
      brief: brief
    });
  }
  render() {
    let { name, brief } = this.state;
    return (
      <div className="col-md-4">
        <div className="panle btn">
          <div className="panle-heading" style={{ backgroundColor: "#E6E6FA" }}>
            <div className="row">
              <h4 className="col-md-10">{name}</h4>{" "}
              <span className="col-md-2">
                <i className="fas fa-cog" />
              </span>
            </div>
            <div className="row">
              <h6 className="col-md-10">{brief}</h6>
            </div>
          </div>
          <div className="panel-body" style={{ backgroundColor: "white" }}>
            <div className="col-md-6">
              <div className="row text-left">
                <svg height="15" width="15">
                  <circle
                    cx="10"
                    cy="10"
                    r="5"
                    strokeWidth="3"
                    fill="#00FFFF"
                  />
                </svg>
                <span> 9 Discussion</span>
              </div>
              <div className="row text-left">
                <svg height="15" width="15">
                  <circle
                    cx="10"
                    cy="10"
                    r="5"
                    strokeWidth="3"
                    fill="#7B68EE"
                  />
                </svg>
                <span> 9 To-do</span>
              </div>
              <div className="row text-left">
                <svg height="15" width="15">
                  <circle
                    cx="10"
                    cy="10"
                    r="5"
                    strokeWidth="3"
                    fill="#F5DEB3"
                  />
                </svg>
                <span> 6 Files</span>
              </div>
            </div>
            <div className="col-md-6">
              <div className="row text-left">
                <svg height="15" width="15">
                  <circle
                    cx="10"
                    cy="10"
                    r="5"
                    strokeWidth="3"
                    fill="#BC8F8F"
                  />
                </svg>
                <span> 1 To-do-list</span>
              </div>
              <div className="row text-left">
                <svg height="15" width="15">
                  <circle
                    cx="10"
                    cy="10"
                    r="5"
                    strokeWidth="3"
                    fill="#DAA520"
                  />
                </svg>
                <span> 2 Comments</span>
              </div>
              <div className="row text-left">
                <svg height="15" width="15">
                  <circle
                    cx="10"
                    cy="10"
                    r="5"
                    strokeWidth="3"
                    fill="#0000FF"
                  />
                </svg>
                <span> 7 Milestones</span>
              </div>
            </div>
          </div>
          <div className="panel-footer text-left" style={{ backgroundColor: "white" }}>
            <svg height="20" width="20" >
              <circle cx="10" cy="10" r="10" strokeWidth="3" fill="#00FFFF" />
            </svg>
            <svg height="20" width="20" style={{marginLeft:5}}>
              <circle cx="10" cy="10" r="10" strokeWidth="3" fill="#7B68EE" />
            </svg>
            <svg height="20" width="20" style={{marginLeft:5}}>
              <circle cx="10" cy="10" r="10" strokeWidth="3" fill="#F5DEB3" />
            </svg>
            <svg height="20" width="20" style={{marginLeft:5}}>
              <circle cx="10" cy="10" r="10" strokeWidth="3" fill="#BC8F8F" />
            </svg>
            <svg height="20" width="20" style={{marginLeft:5}}>
              <circle cx="10" cy="10" r="10" strokeWidth="3" fill="#DAA520" />
            </svg>
            <svg height="20" width="20" style={{marginLeft:5}}>
              <circle cx="10" cy="10" r="10" strokeWidth="3" fill="#0000FF" />
            </svg>
          </div>
        </div>
      </div>
    );
  }
}
