import React from "react";

export default class SideBar extends React.Component {
  constructor(props) {
    super(props);
    this.click = this.click.bind(this);
  }

  click() {
    
  }

  render() {
    return (
      <div className="well"  style={{position:"fixed"}}>
      <div className="row">
          <input
            type="button"
            value="Home"
            className="btn btn-block btn-primary"
            onClick={this.props.click}
          />
        </div>
        <div className="row">
          <input
            type="button"
            value="HR"
            className="btn btn-block btn-primary"
            onClick={this.props.click}
          />
        </div>
        <div className="row">
          <input
            type="button"
            value="CRM"
            className="btn btn-block btn-primary"
            onClick={this.props.click}
          />
        </div>
        <div className="row">
          <input
            type="button"
            value="Accounting"
            className="btn btn-block btn-primary"
            onClick={this.props.click}
          />
        </div>
        <div className="row">
          <input
            type="button"
            value="Projects"
            className="btn btn-block btn-primary"
            onClick={this.props.click}
          />
        </div>
      </div>
    );
  }
}
